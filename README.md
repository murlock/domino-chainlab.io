Domino-Chain
============

Rearrange dominoes on different platforms to start a chain reaction.

Domino-Chain is a puzzle game where you have to rearrange
dominoes on different platforms to start a chain reaction that
makes all dominoes topple over. There are many strange types
of dominoes, such as the Ascender which will rise to the
ceiling when pushed, or the Exploder which will blast a hole
into the platform it stands on.

Domino-Chain is a faithful reincarnation of the game Pushover
originally published by Ocean in 1992. Compared to Pushover,
Domino-Chain has some new levels, some additional domino
types, better graphics in higher resolution and high-quality
music. On top of that, you can load and play the original
levels from Pushover if you have a copy of it.

This game is free software and created by volunteers. Even
though it is in a pretty good state, there is a lot to
improve. If you like Domino-Chain, please consider to join the
team and to help with translations, levels, graphics and more.

Install
=======

Debian, Mint, Ubuntu
--------------------

On Debian, Mint, Ubuntu and similar systems, Domino-Chain
should already be available. Just install the package
"domino-chain".

Otherwise, please build from source.

Windows
-------

Since Windows lacks a proper package management system, you
need to download and unpack the package by hand:

  Domino-Chain for Windows
  <https://gitlab.com/domino-chain/domino-chain.gitlab.io/uploads/18740047d36c5f760fde34896a2ce1c4/Domino-Chain_1.1_for_Windows.zip>

Mac OS X
--------

This system is not yet supported, but it should be possible
without too much hassle. Please get in contact with us if
you'd like to volunteer.

Source
------

If your operating system is not listed above, or you want to
try the latest development version, you can build from source.
You need:

*Tools:*
GCC/g++, gettext, Git, ImageMagick, Make, pkgconf,
POV-Ray, sed, shellcheck

*Libraries:*
Boost filesystem, Boost system, FriBidi, LUA, libpng,
SDL2, SDL2_image, SDL2_mixer, SDL2_ttf, zlib

*Fonts:*
FreeSans

On Debian-based systems, this means:

    sudo apt-get install fonts-freefont-ttf g++ gettext imagemagick libboost-filesystem-dev libboost-system-dev libfribidi-dev liblua5.2-dev libpng-dev libsdl2-dev libsdl2-image-dev libsdl2-mixer-dev libsdl2-ttf-dev make pkgconf povray shellcheck zlib1g-dev

Clone the Git repository, build the game and run it as follows:

    git clone https://gitlab.com/domino-chain/domino-chain.gitlab.io.git domino-chain
    cd domino-chain
    make
    ./domino-chain

Have fun!

Play
====

Rules
-----

You control a figure that can walk along platforms which are
connected with ladders. On those platforms there are dominoes
that will fall according to certain rules. Your task is to
rearrange the dominoes so you can start a chain reaction that
makes all dominoes topple over. The rules are:

*Domino types:*
There are many different dominoes that behave differently
when pushed. Some fall, some not, some wait a bit before
they fall, some raise, some topple until they meet an
obstacle.

*All fall, no crash:*
All dominoes (except for the blocker) must fall and none
must crash into another. They are allowed to fall off the
screen, though.

*Trigger and exit door:*
The trigger domino must fall as last domino. Only then it
triggers the exit door to open. When you enter the exit door
the level has been completed.

*Disallowed moves:*
You may rearrange as many dominoes as you want, except for
the trigger. You must not place dominoes in front of the
doors, except for the vanishing domino.

*One push:*
You may push only once to start a chain reaction with the
dominoes leading to the fall of all of them.

*Time limit:*
All this has to be done within a time limit. This limit is
usually generous, but not always.

There is an in-game help as well as and introductory levels
that show how all the dominoes work.

Dominoes
--------

The following types of dominoes exist:

*Standard, completely yellow:*
There is nothing special with this stone, it behaves like
a regular domino and falls when pushed.

*Blocker, completely red:*
This domino can't fall over, so it is the only kind
of stone that may still be standing when the level is
solved. Dominoes falling against this stone will bounce
back, if possible.

*Tumbler, big red stripe:*
This domino will stand up again after falling and will
continue to tumble until it hits an obstacle or rests
against another stone.

*Delay stone, diagonally divided:*
This domino will take some time until it falls, when
it is pushed. Dominoes falling against this stone will
bounce back and later this stone will fall.

*Splitter, horizontally divided:*
This stone will split into two stones, one falling to
the left and the other falling to the right. The splitter
can't be pushed. It must be split by a stone falling onto
it from above. A pile of rubbish falling into it also
activates this domino.

*Exploder, vertically divided:*
This stone will blast a gap into the platform it is
standing on, when it is pushed.  Neither the figure nor
the pushing domino are harmed by that, the pushing domino
will fall into the gap.

*Bridger, 1 horizontal strip:*
The bridger will try to connect the edge it is standing
on with the next edge, if it is close enough, if not it
will simply fall into the gap.

*Vanisher, 2 horizontal strips:*
The Vanisher will disappear as soon as it lies flat on
the ground. This is the only stone you may place in front
of doors.

*Trigger, 3 horizontal strips:*
This stone will open the exit door, as soon as it lies
completely flat and all other conditions are met (see
above). This is the only stone that you are not allowed
to move around.

*Ascender, vertical strip:*
This stone will start to rise as soon as it is pushed. It
will rise until is hits the ceiling, then it will start to
flip into the direction it was initially pushed. When you
fall into a gap while holding this stone it will also rise
and stay at the ceiling until pushed.

*Quantum, diagonally divided:*
All dominoes of this type will fall together as if they
were quantum entangled. There are two types of quantum
dominoes with inverted color markings. Those of the same
type will fall into the same direction, the others will
fall into the opposite direction. This is a new type of
dominoes not present in the original Pushover game.

Controls
--------

The figure is controlled using the cursor keys and space. Use
the space key to pick up the domino behind the figure or to
place it down where you are currently standing. To push press
first up to let the figure enter the row of dominoes. Then
simultaneously press space and either left or right cursor key
depending on whether you want to push the domino to your left
or your right.

Hints
-----

If you don't know where to start in a level, simply push a
stone and observe what happens. This helps very often to get
a general idea how to solve a level and where the problem is.

If you forgot which domino has what kind of special property
press F1 to get a short help. This window also displays a
short hint, once the time of the level is out.

The first levels introduce you to the dominoes. Here you
can explore how the different dominoes behave in different
situations.

Contribute
==========

Contact
-------

You can file bug reports, discuss ideas for new features and
offer contributions on the

  Domino-Chain Issue Tracker
  <https://gitlab.com/domino-chain/domino-chain.gitlab.io/issues>

which is the central point to get in contact with the project.
There, you'll also find links to the code repository, and so
on. If you need to contact us privately, please contact
Andreas Röver via roever at users dot sourceforge dot net.

Authors
-------

*Andreas Röver:*
Project founder, main programming and reverse engineering.

*Volker Diels-Grabsch:*
Text-based level format, build system and code assorted bugfixes and patches.

*Roberto Lorenz:*
Music composition and arrangement.

*Harald Radke:*
Theme graphics.

Files
-----

Domino-Chain places a few files on your hard-disc in everyday
running. Those files will be placed in your home directory.
The exact location depends on your operating system.

*Location on Unix systems:*
~/.local/share/domino-chain

*Location on Windows:*
My Documents\domino-chain

The following files are saved:

*solved.txt:*
This file contains checksums of all the levels that you have
successfully solved. In the level selection dialogue those
levels contain a mark. If you loose this file those marks
are gone.

**.rec:*
These files contain recordings of activities within a level.
They are automatically created whenever you solve a level
but you can also actively make a recording by pressing 'r'
while you play a level. When you observe something strange
while playing, make a recording and send it to us. Also when
the game crashes a recording will be saved. You can delete
these files whenever you want. You can distinguish the
recordings by the prefix in their name. "Sol" stands for
solved levels, "Man" for manually created recordings and
"Err" for recordings made when the program crashed.

Level Designers
---------------

Domino-Chain will eventually get a level editor, but right now
it doesn't. However, all levels are stored in a readable plain
text format, so you can create and modify levels directly with
a text editor.

To get your levels officially included in the Domino-Chain
project, you need to adhere to the following rules:

*License:*
They need to be put under the GPL, or a compatible license.
Otherwise inclusion is legally not possible. Copyright stays
with you, of course.

*Complete sets:*
Please contribute only complete sets and no single levels.
They don't need to be long, 10 levels is enough, but this
way you can keep up a constant scheme and theme logic of the
levels.

*Solutions:*
You absolutely must provide a recording of one possible
solution to each level. That solution is not within the
distribution, just within the source code repository. It is
used to ensure that the solution of your level is still
possible after we made changes to the program. This way we
limit possible frustration.

*Non-compressed:*
Do only send non-compressed level sets. We need those for
the inclusion in the source. And we need those for possible
future updates.

*Index:*
Use the index file to reorder levels, don't rename the
files. This way inserting a level becomes very easy.

Graphics
--------

We have graphics for most themes, but not for all. Those
themes do have some foreground graphics, but their background
tiles are all black. You are very much invited to create and
to improve the graphics. Even more important, though, is the
creation of a new main figure. In either case, please contact
us if you are interested.

The backgrounds are made out of 20x13 tiles. Each tile has a
size of 40x48 pixel. The reason for that is the non square
pixel of the 320x200 resolution of the original game. For each
theme there is a PNG image file containing all the blocks that
may be used by the levels. To make it possible to place the
blocks more freely into the PNG file a LUA file accompanies
the image. This LUA file contains the block positions of all
the used blocks.

It is already implemented to use transparency within the
blocks. Right now  you can stack up to 8 layers, but if
necessary this can be made dynamic.

It is also planned to have something like animated tiles, but
they have to be kept at a low count. Not too many frames and
not too many animations. They are not intended to make the
background dynamic, but to rather be a little finishing touch
to the graphics. Possibilities are trees that move from time
to time in a breeze, a bird that sails through the sky from
time to time, and so on.

The figure is more complicated. The image figure.png contains
all possible animation images for the figure, one animation
below the other. On request we can provide an additional GIMP
image that contains in separate layers possible surroundings
of the figure in different animation frames (like ladders,
steps, ground, a carried domino, and so on). We will happily
provide that image to an interested artist.

Changelog
=========

Release 1.1
-----------

*Improved gameplay:*
The automatic set down of dominoes is disabled, as this
mechanism is irritating for novice and young players.

*Port to SDL 2:*
The whole project is ported from SDL 1 to SDL 2, making the
code base more future-proof.

*Improved build system:*
The build system now provides a fully automated cross build
for Windows using MXE. Along the way, several issues regarding
cross builds are fixed.

*Typos and translations:*
Multiple spelling mistakes are fixed and translations are
updated.

Release 1.0
-----------

*Initial release:*
This is the initial version of the Domino-Chain project.

Credits
=======

We want to thank the developers of the original game for
making such a nice little game.
